# -*- coding: utf-8 -*-
__author__ = "Sommily"
import time
import json
import requests
from recruit.models import User, Interaction


print(User.objects.filter(del_state=1))

headers = {
    "Host": "open.taou.com",
    "Accept": "*/*",
    "Accept-Language": "zh-Hans-CN;q=1, en-CN;q=0.9",
    "User-Agent": "User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Mobile/14G60/{iPhone8,2} [iOS 10.3.3]/MaiMai 4.17.2(4.17.2)"
}

url_params = {
    "fids": "1000000002,1562267",
    "access_token": "1.ff2d4ec3c8051a9b8ec1ef9cf03ed0fc",
    "appid": "4",
    "channel": "AppStore",
    "density": "3",
    "device": "iPhone8",
    "ignore_default_p": 0,
    "imei": "55AB216B-BBB5-4BB8-9A6A-84372C762546",
    "limit": "10",
    "net": "wifi",
    "open": "icon",
    "push_permit": 0,
    "thumb_size": 244,
    "vc": "10.3.3",
    "version": "4.17.2"
}


def download_users(uid="108420937", maimai_id="1562267"):
    url = "https://open.taou.com/maimai/feed/v3/gets"
    page = 0
    while True:
        param = url_params
        param["fids"] = "1000000002,{}".format(maimai_id)
        param["ts"] = int(time.time() * 1000)
        param["u"] = uid
        param["page"] = page
        response = requests.get(url=url, params=param, headers=headers)
        result = json.loads(response.content)
        for feed in result["feeds"]:
            try:
                if feed["type"] != 2:
                    continue
                user_info = feed["main"]["u2"]
                user = User.objects.filter(maimai_id=str(user_info["id"])).first()
                if user is not None:
                    user.rank = user_info["rank"]
                    user.save()
                    continue

                User.new_save(uid=user_info["id"],
                              name=user_info["name"],
                              rank=user_info["rank"],
                              job=user_info["position"],
                              company=user_info["company"],
                              location=user_info["loc"],
                              avatar=user_info["avatar"],
                              company_industry=user_info["line4"].split("|")[0],
                              sex=user_info["gender"])
            except Exception as e:
                print(e)

        if result["remain"] == 1:
            page += 1
        else:
            break

    return True


def download_point(uid, user2):
    try:
        url = "https://open.taou.com/maimai/feed/v3/user"
        page = 0

        while True:
            param = url_params
            param["only_topic"] = 1
            param["only_realname_status"] = 1
            param["ts"] = int(time.time() * 1000)
            param["u"] = uid
            param["u2"] = user2.maimai_id
            param["page"] = page

            response = requests.get(url=url, params=param, headers=headers)
            result = json.loads(response.content)
            json.dump(result, open("data/point/{}_{}.json".format(user2.maimai_id, page), 'w'))
            for feed in result["feeds"]:
                Interaction.new_save(uid=user2.maimai_id,
                                     comment_id=feed["id"],
                                     like=feed["likes"]["n"],
                                     spread=feed["spreads"]["n"],
                                     comment=feed["comments"]["n"])
            if result["remain"] == 1:
                page += 1
            else:
                break

        user2.finish_download_point()
        return True

    except Exception as e:
        print(e)
        return False


def start():
    users = User.objects.filter(point_download=0).all()
    for user in users:
        user.start_download_point()
    for user in users:
        print(download_point("108420937", user))


def start_user():
    users = User.objects.filter(rank=0).all()
    for user in users:
        maimai_id = user.maimai_id
        download_users("108420937", maimai_id)
