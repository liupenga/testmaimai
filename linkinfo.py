#coding:utf-8
import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")

'''
Django 版本大于等于1.7的时候，需要加上下面两句
import django
django.setup()
否则会抛出错误 django.core.exceptions.AppRegistryNotReady: Models aren't loaded yet.
'''


if django.VERSION >= (1, 7):
    django.setup()



from recruit.models import User, Interaction
from collections import Counter
import numpy as np



# def getLocationRank():
#     users = User.objects.filter(del_state=1).values_list("location")
#     return users
# a = getLocationRank()

# def get_company_industry():
#     users = User.objects.filter(del_state=1).values_list("company_industry")
#     return users
# users = get_company_industry()
# for row in Counter(users).items():
#     print(row[0], row[1])
def getTopFive():
    users = User.objects.filter(del_state=1, sex=1).order_by("-rank")[0:5]
    return users
users = getTopFive()
man_img = [(row.avatar, row.rank) for row in users]
with open("man.txt", "a") as f:
    for line in man_img:
        f.write("%s,%s\n" % (line[0], line[1]))
print("完成。")

# print(dir(Counter(a)))
# data = []
# for x, y in Counter(a).items():
#     data.append((x, y))
# print(data)
#     # print(x, y)

# with open("location.txt", "a") as f:
#     for line in Counter(a).items():
#         f.write("%s,%s\n" % (line[0],line[1]))
# print("完成")
# data = []
# with open("location.txt", "r") as f:
#     for line in f:
#         line_lst = line.split(",")
#         print(line)
        # data.append((line_lst[0], line_lst[1]))
# print(data)
    

# def getAvg(app_id):
#     from recruit.models import User, Interaction
#     interactions_list = Interaction.objects.filter(app_uuid=app_id)
#     length = interactions_list.count()
#     like_list = interactions_list.values_list("like")
#     spread_list = interactions_list.values_list("spread")
#     comment_list = interactions_list.values_list("comment")
#     like_avg = np.mean(like_list)#like平均值
#     like_variace = np.std(like_list)#标准差
#     spread_avg = np.mean(spread_list)#spread平均值
#     spread_variance = np.std(spread_list)#标准差
#     comment_avg = np.mean(comment_list)#comment平均值
#     comment_variance = np.std(comment_list)#标准差
#     return "%s,%s,%s,%s,%s,%s,%s" % (like_avg, like_variace, spread_avg, spread_variance, comment_avg, comment_variance, length)



# def info():
#     from recruit.models import User, Interaction
#     users = User.objects.filter(del_state=1)
#     data = []
#     for user in users:
#         if getAvg(user.app_uuid)[-1] == 0:
#             continue
#         # print(user.uuid)
#         data.append("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (user.app_uuid, 
#                      user.sex, 
#                      user.location, 
#                      user.job, 
#                      user.company, 
#                      user.company_industry, 
#                      user.friends, 
#                      user.opposite_friends,
#                      user.rank, 
#                      getAvg(user.app_uuid)))
#     return data

# with open("a.txt", "a") as f:
#     for line in info():
#         print(1)
#         try:
#             f.write("%s\n" % line)
#         except:
#             f.write("-------------------")
# print("完成！")
        # data.append((user.app_uuid, 
        #              user.sex, 
        #              user.location, 
        #              user.job, 
        #              user.company, 
        #              user.company_industry, 
        #              user.friends, 
        #              user.opposite_friends,
        #              user.rank, 
        #              getAvg(user.app_uuid)),)
    # return data

# print(info())




        
        
