#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 16 22:59:02 2017

@author: ladny
"""

import pandas as pd
import numpy as np
import preprocess
filename = "/home/ladny/maimai/testmaimai/a.txt"
def construct_pandas(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    uid = []
    gender = []
    location = []
    title = []
    company = []
    occupation = []
    influence = []
    like_mean = []
    like_std = []
    spread_mean = []
    spread_std = []
    comment_mean = []
    comment_std = []
    total_count = []
    for line in lines:
        data = line.split(",")
        try:
            #clean the strcture
            _uid = int(data[0].strip("-"))
            _gender = int(data[1])
            _location = data[2]
            _title = data[3]
            _company = data[4]
            _occupation = data[5]
    #        _, 
    #        _, 
            _influence = int(data[8])
            _like_mean = float(data[9])
            _like_std = float(data[10])
            _spread_mean = float(data[11])
            _spread_std = float(data[12])
            _comment_mean = float(data[13])
            _comment_std = float(data[14])
            _total_count = int(data[15].strip('\n'))
        except Exception as e:
            print(e)
            print(len(data))
            print(data)
            continue
        uid.append(_uid)
        gender.append(_gender)
        location.append(_location)
        title.append(_title)
        company.append(_company)
        occupation.append(_occupation)
        influence.append(_influence) 
        like_mean.append(_like_mean) 
        like_std.append(_like_std) 
        spread_mean.append(_spread_mean) 
        spread_std.append(_spread_std) 
        comment_mean.append(_comment_mean) 
        comment_std.append(_comment_std)
        total_count.append(_total_count)
    
    df = pd.DataFrame({"uid": np.array(uid),
        "gender": pd.Categorical(gender),
        "location": pd.Categorical(location),
        "title": pd.Categorical(title),
        "company": pd.Categorical(company),
        "occupation": pd.Categorical(occupation),
        "influence" : np.array(influence),
        "like_mean" : np.array(like_mean),
        "like_std" : np.array(like_std),
        "spread_mean" : np.array(spread_mean),
        "spread_std" : np.array(spread_std),
        "comment_mean" : np.array(comment_mean),
        "comment_std" : np.array(comment_std),
        "total_count" : np.array(total_count)})
    return df

location_names = preprocess.read_categories(location_file, 10)
def calc_stats():
    df = construct_pandas(filename)
    influnece = df[df.influence > 0] #8588 ppl
    mean, std = influnece.mean(axis=0)['influence'], influnece.std(axis=0)['influence']#mean = 155, std = 258
    male = influnece[influnece.gender == 1] #6919 ppl
    mean, std = male.mean(axis=0)['influence'], male.std(axis=0)['influence'] #mean = 159, std = 269
    female = influnece[influnece.gender == 2] #1577 ppl
    mean, std = female.mean(axis=0)['influence'], female.std(axis=0)['influence'] #mean = 144,  std = 215
    for location_name in location_names:
        location = influnece[influnece.location == location_name] #4528 ppl
        mean, std = location.mean(axis=0)['influence'], location.std(axis=0)['influence'] #mean = 185,  std = 287
        print("{} count ={},  mean = {}, std = {}".format(location_name, len(location), mean, std))