$.getJSON("/static/json/pie.json",function(res){
    var resdata = res
    var labelTop = {
        normal : {
            label : {
                show : false,
                position : 'center'
            },
            labelLine : {
                show : false
            }
        },
        emphasis: {
            label : {
                show : true,
                formatter : function (params){
                    console.log(params)
                    return params.name + '\n' + params.value + '%'
                }
            }
        }
    };
    var radius = [40, 55];
    option = {
        legend: {
            x : 'center',
            y : 'center',
            data:[
                '工程师', '管理', '高层', '设计', '销售', '助理', '学生', '金融', '媒体', '咨询', '其他'
            ]
        },
        title : {
            text: '各类职业在不同城市中所占比重',
            subtext: '原始数据来自脉脉',
            x: 'center'
        },
        toolbox: {
            show : true,
            feature : {
                dataView : {show: true, readOnly: false},
                magicType : {
                    show: true, 
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            width: '20%',
                            height: '30%',
                            itemStyle : {
                                normal : {
                                    label : {
                                        formatter : function (params){
                                            return 'other\n' + params.value + '%\n'
                                        },
                                        textStyle: {
                                            baseline : 'middle'
                                        }
                                    }
                                },
                            } 
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        tooltip : {
            position: 'bottom',
            formatter : function(params) {
                return params.seriesName
            }
        },
    series : [
            {
                type : 'pie',
                center : ['10%', '30%'],
                radius : radius,
                x: '0%', // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['30%', '30%'],
                radius : radius,
                x:'20%', // for funnel
                name: '上海',
                data : [
                    {name: resdata.shanghai[0].name, value: resdata.shanghai[0].value, itemStyle : labelTop},
                    {name: resdata.shanghai[1].name, value: resdata.shanghai[1].value, itemStyle : labelTop},
                    {name: resdata.shanghai[2].name, value: resdata.shanghai[2].value, itemStyle : labelTop},
                    {name: resdata.shanghai[3].name, value: resdata.shanghai[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['50%', '30%'],
                radius : radius,
                x:'40%', // for funnel
                name: '广州',
                data : [
                    {name: resdata.guangzhou[0].name, value: resdata.guangzhou[0].value, itemStyle : labelTop},
                    {name: resdata.guangzhou[1].name, value: resdata.guangzhou[1].value, itemStyle : labelTop},
                    {name: resdata.guangzhou[2].name, value: resdata.guangzhou[2].value, itemStyle : labelTop},
                    {name: resdata.guangzhou[3].name, value: resdata.guangzhou[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['70%', '30%'],
                radius : radius,
                x:'60%', // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['90%', '30%'],
                radius : radius,
                x:'80%', // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['10%', '70%'],
                radius : radius,
                y: '55%',   // for funnel
                x: '0%',    // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['30%', '70%'],
                radius : radius,
                y: '55%',   // for funnel
                x:'20%',    // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['50%', '70%'],
                radius : radius,
                y: '55%',   // for funnel
                x:'40%', // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['70%', '70%'],
                radius : radius,
                y: '55%',   // for funnel
                x:'60%', // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            },
            {
                type : 'pie',
                center : ['90%', '70%'],
                radius : radius,
                y: '55%',   // for funnel
                x:'80%', // for funnel
                name: '北京',
                data : [
                    {name: resdata.beijing[0].name, value: resdata.beijing[0].value, itemStyle : labelTop},
                    {name: resdata.beijing[1].name, value: resdata.beijing[1].value, itemStyle : labelTop},
                    {name: resdata.beijing[2].name, value: resdata.beijing[2].value, itemStyle : labelTop},
                    {name: resdata.beijing[3].name, value: resdata.beijing[3].value, itemStyle : labelTop},
                ]
            }
        ]
    };

    var myChart= echarts.init(document.getElementById('main'));
    myChart.setOption(option);
})