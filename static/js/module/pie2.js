$.getJSON("/static/json/pie2.json",function(res){
    var resdata = res
    option = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        title : {
            text: '影响力分布情况',
            subtext: '原始数据来自脉脉',
            x: 'center'
        },
        series: [{
            name: '影响力占比',
            type: 'pie',
            radius: '68%',
            center: ['50%', '60%'],
            clockwise: false,
            data: resdata,
            label: {
                normal: {
                    textStyle: {
                        color: '#999',
                        fontSize: '14px',
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            itemStyle: {
                normal: {
                    borderWidth: 4,
                    borderColor: '#ffffff',
                },
                emphasis: {
                    borderWidth: 0,
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }],
        color: [
            '#00acee',
            '#52cdd5',
            '#79d9f1',
            '#a7e7ff',
            '#c8efff'
        ]
    }
    
    var myChart= echarts.init(document.getElementById('main'));
    myChart.setOption(option);
})