#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 16 15:23:28 2017

@author: qianyizhang
"""
#from sklearn import preprocessing
import jieba
import jieba.analyse
#import pandas as pd
import numpy as np
import operator

from sklearn.ensemble import ExtraTreesRegressor, RandomForestRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.linear_model import LinearRegression

import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

filename = "/home/ladny/maimai/testmaimai/user2.csv"
f =  "/home/ladny/maimai/testmaimai/a.txt"
location_file = "/home/ladny/maimai/testmaimai/location.txt"
occupation_file = "/home/ladny/maimai/testmaimai/industry.txt"

##########
##block 0, preprocess the titles
##########
def read_title_data(filename):
    """
    rowbased data should be following the format
    UID, gender, location, company, title, feed (), interactions (total, by)
    """
    titles = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            try:
                title = line.split(",")[6].strip(' "')
#                print(title)
            except:
#                print("!!!!!!!!!!!!  {}".format(line))
                pass
#            print(line)
            titles.append(title)
        
    return titles


def label_title_list(titles, title_list = [], unused_list = []):
    """
    handcrafted grouping of titles
    """
    index = 0
    while index < len(titles):
        new_title = titles[index]
        index += 1
        if new_title in [title for title_group in title_list for title in title_group]:
            continue
        if new_title in unused_list:
            continue
        for group_index, title_group in enumerate(title_list):
            print("{}: {}".format(str(group_index), str(title_group)))
        new_index = input(new_title + " ")
        try:
            new_index = int(new_index)
            title_list[new_index].append(new_title)
        except:
            if new_index == "n":
                title_list.append([new_title])
            elif new_index == "q":
                break
            else:
                unused_list.append(new_title)
    return title_list, unused_list
    
def save_title_list(title_list, filename = "/home/ladny/maimai/testmaimai/title_list.csv"):
    with open(filename, 'w') as f:
        f.write("\n".join([",".join(title_group) for title_group in title_list]))
        
def load_title_list(filename = "/home/ladny/maimai/testmaimai/title_list.csv"):
    title_list = []
    with open(filename, 'r') as f:
        for line in f.read().split('\n'):
            title_list.append(line.split(","))
    return title_list

def title_process():
    titles = read_title_data(filename) ##get all the titles
    freq_titles = jieba.analyse.extract_tags(",".join(titles),200) ##sort out top 200 frequent words
    title_list = label_title_list(freq_titles)
    save_title_list(title_list)
#############
##block1: 
#############
def read_all_data(filename):
    """
    rowbased data should be following the format
    UID, gender, location, title, company, 0, 0, influence, 
        like(mean), like(std), spread(mean), spread(std), 
        comment(mean), comment(std), total count
    """
    def normalize(key):
        key.strip(' "\n')
        try:
            key = float(key)
        except:
            pass
        return key
    with open(filename, 'r') as f:
        lines = f.readlines()
#        header = [key.strip(' "') for key in lines[0].split(",")]
        data = []
        for line in lines[0:]:
            try:
                user_data = [normalize(datum) for datum in line.split(",")]
            except:
                continue
            if len(user_data) == 16:
                data.append(user_data)
    return data

def read_categories(filename, topK = -1):
    with open(filename, 'r') as f:
        cities = f.readlines()
    city_dict = {c.split(",")[0]: int(c.split(",")[1].strip("\n")) for c in cities}
    sorted_cities = sorted(city_dict.items(), key=operator.itemgetter(1), reverse = True)
#    print(sorted_cities)
    return [t[0] for i, t in enumerate(sorted_cities) if ((i < topK) or (topK == -1))]
    
def catgorical_to_one_hot(value, category, nested = False):
    one_hot = np.zeros(len(category)+1)
    if nested:
        other = True
        for index, cat in enumerate(category):
            for key_word in cat:
                if key_word in str(value):
                    one_hot[index] = 1
                    other = False
        if other:
            one_hot[-1] = 1
    else:
        if value in category:
            index = category.index(value)
            one_hot[index] = 1
        else:
            one_hot[-1] = 1
    return one_hot

def catgorical_to_index(value, category, nested = False):
    if nested:
        for index, cat in enumerate(category):
            for key_word in cat:
                if key_word in str(value):
                    return index
        return len(category)
    else:
        if value in category:
            index = category.index(value)
            return index
        return len(category)

def get_occupation_list(data):
    pass
#############
##block1: 
#############     
def prepare_train_influence(filename):
    data = read_all_data(filename)
    train_data = None
    train_label = None
    headers = ["gender"]
    [headers.append(i) for i in  read_categories(location_file, 10)]
    headers.append("other_location") 
    jobs = "engineer, manager, ceo, designer, sales, support_functions, students, businessman, media, teacher".split(", ")
    [headers.append(i) for i in  jobs]
    headers.append("other_job") 
    for ppl in data:
        if ppl[8] > 0:
            #has tweeted something
            gender = np.array([ppl[1]-1]) 
            location = catgorical_to_one_hot(ppl[2], read_categories(location_file, 10))
            title = catgorical_to_one_hot(ppl[3], load_title_list(), True)
#            occupation = catgorical_to_one_hot(ppl[5], read_categories(occupation_file, 10))
#            print(occupation)
#            feature_list = np.hstack((gender, location, title, occupation))
            feature_list = np.hstack((gender, location, title))
#            label_list = np.hstack((ppl[-7], ppl[-5], ppl[-3], ppl[-1])) #like_mean, spread_mean, comment_mean, totoal_count
            label_list = np.array([ppl[8]])
            if type(train_data) == type(None):
                train_data = feature_list
                train_label = label_list
            else:
                train_data = np.vstack((train_data, feature_list))
                train_label = np.vstack((train_label, label_list))
    return train_data, train_label, headers

def prepare_train_total_count(filename):
    """
    tweet count
    """
    data = read_all_data(filename)
    train_data = None
    train_label = None
    train_label2 = None
    for ppl in data:
        if ppl[-1] > 0:
            #has tweeted something
            gender = np.array([ppl[1]-1]) 
            location = catgorical_to_one_hot(ppl[2], read_categories(location_file, 10))
            title = catgorical_to_one_hot(ppl[3], load_title_list(), True)
#            occupation = catgorical_to_one_hot(ppl[5], read_categories(occupation_file, 10))
#            feature_list = np.hstack((gender, location, title, occupation))
            feature_list = np.hstack((gender, location, title))
            label_list = np.hstack((ppl[-7], ppl[-5], ppl[-3], ppl[-1])) #like_mean, spread_mean, comment_mean, totoal_count
            influence_list = np.array([ppl[8]])
            if type(train_data) == type(None):
                train_data = feature_list
                train_label = label_list
                train_label2 = influence_list
            else:
                train_data = np.vstack((train_data, feature_list))
                train_label = np.vstack((train_label, label_list))
                train_label2 = np.vstack((train_label2, influence_list))
    return train_data, train_label, train_label2

#############
##evaluation helper
#############   
def plot_label(label):
    np.random.seed(0)
    num_bins = 50
    
    fig, ax = plt.subplots()
    
    # the histogram of the data
    n, bins, patches = ax.hist(label, num_bins, normed=1)
    
    mu = np.mean(label)
    sigma = np.sqrt(np.std(label))
    # add a 'best fit' line
    y = mlab.normpdf(bins, mu, sigma)
    ax.plot(bins, y, '--')
    ax.set_title(r'Histogram of IQ: $\mu=100$, $\sigma=15$')
    
    # Tweak spacing to prevent clipping of ylabel
    fig.tight_layout()
    plt.show()

def plot_pie(labels = [], sizes = []):
#    labels = 'Frogs', 'Hogs', 'Dogs', 'Logs'
#    sizes = [15, 3, 45, 10]
    sizes = np.array(sizes)
#    explode = (0, 0.1, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')
    explode = np.zeros(sizes.shape)
    explode[sizes>0.1] = 0.1
    fig1, ax1 = plt.subplots()
#    fig = plt.f
    plt.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    
    plt.show()
    fig1.savefig('/home/ladny/maimai/testmaimai/pie.png', dpi = 180)
    
def mse(clf, train_data, train_label):
    p = clf.predict(train_data).reshape(train_label.shape)
    diff = train_label-p
    sq = diff*diff
    mse_error = np.sqrt(np.sum(sq)/len(sq))
    return mse_error
#    
#train_data, train_label = prepare_train_influence(f)
#inlier = np.where(train_label < 1000)[0]
##inlier = np.where(train_data[:,0] == 0)[0]
#clf = ExtraTreesRegressor(n_estimators=50, random_state=0, verbose=1).fit(train_data[inlier, :], train_label[inlier])
##clf = GaussianProcessRegressor().fit(train_data, train_label)
#s = clf.score(train_data, train_label)
#s2 = clf.score(train_data[inlier, :], train_label[inlier])
#
#clf3 = LinearRegression().fit(train_data[inlier, :], train_label[inlier])
#ss = clf3.score(train_data, train_label)
#ss2 = clf3.score(train_data[inlier, :], train_label[inlier])
#
#print(clf.predict(train_data[0:5]))
#print(train_label[0:5])
#print(mse(clf, train_data[inlier, :], train_label[inlier]))


#############
##block2: start training and make conclusion
#############   
def influence():    
    train_data, train_label, headers = prepare_train_influence(f)
    inlier = np.where(train_label < 1000)[0]
#    inlier = np.where(train_data[:,0] == 0)[0]
    clf = ExtraTreesRegressor(n_estimators=50, random_state=0).fit(train_data[inlier, :], train_label[inlier])
    #clf = GaussianProcessRegressor().fit(train_data, train_label)
    s = clf.score(train_data, train_label)
    s2 = clf.score(train_data[inlier, :], train_label[inlier])
    
    print("score: {}, {}".format(s, s2))
    feature_importance = clf.feature_importances_ 
    print(feature_importance)
#    print(clf.predict(train_data[0:5]))
#    print(train_label[0:5])
    print(mse(clf, train_data, train_label))
    print(mse(clf, train_data[inlier, :], train_label[inlier]))
    """
    male beijing engineering = 124.07936508
    male beijing manager = 185.84615385
    male beijing ceo = 327.22222222
    female beijing ceo = 310
    female shanghai engineering = 83.23809524
    female shanghai manager = 147.7]
    female shanghai ceo = 274
    """
    return clf
    
def tweet():    
    train_data, train_label, train_label2= prepare_train_total_count(f)
    clf = ExtraTreesRegressor(n_estimators=10, random_state=0).fit(train_data, train_label)
    #clf = GaussianProcessRegressor().fit(train_data, train_label)
    s = clf.score(train_data, train_label)
    
    print("score: {}".format(s))
    feature_importance = clf.feature_importances_ 
    print(feature_importance)
    print(mse(clf, train_data, train_label))
    ##model influence against tweets
    clf2 = ExtraTreesRegressor(n_estimators=10, random_state=0).fit(train_label, train_label2)
    s = clf2.score(train_label, train_label2)
    print("score: {}".format(s))
    feature_importance = clf2.feature_importances_ 
    print(feature_importance)
    print(mse(clf2, train_label, train_label2))
    
def calc_stats():
    data = read_all_data(f)
    engineer_sum = 0
    engineer_count = 0
    manager_sum = 0
    manager_count = 0
    ceo_sum = 0
    ceo_count = 0
    for ppl in data:
        influence = ppl[-1]
        if influence > 0:
            #influence
            influence = ppl[-7]
            title = catgorical_to_index(ppl[3], load_title_list(), True)
#            print(title)
            if title == 0:
                engineer_sum += influence
                engineer_count += 1
            elif title == 1:
                manager_sum += influence
                manager_count += 1
            elif title == 2:
                ceo_sum += influence
                ceo_count += 1
    print ("engineer_count: {}, {}".format(engineer_count, engineer_sum/engineer_count))
    print ("manager_count: {}, {}".format(manager_count, manager_sum/manager_count))
    print ("ceo_count: {}, {}".format(ceo_count, ceo_sum/ceo_count))
    """
    influence
    engineer_count: 3550, 112.10845070422535
    manager_count: 2733, 161.51994145627515
    ceo_count: 1007, 306.6961271102284
    
    tweet
    engineer_count: 132, 5.924242424242424
    manager_count: 114, 8.368421052631579
    ceo_count: 28, 12.357142857142858
    
    comments
    engineer_count: 132, 0.6484133445856968
    manager_count: 114, 0.7424460487814944
    ceo_count: 28, 1.613287076253857
    
    spread
    engineer_count: 132, 1.3590975854730258
    manager_count: 114, 0.9101606219718315
    ceo_count: 28, 0.7230214094706928
    
    likes
    engineer_count: 132, 5.804147814794092
    manager_count: 114, 7.117375095620931
    ceo_count: 28, 12.956338548673571
    """
if __name__ == "__main__":
#    train_data, train_label = prepare_train_total_count(f)
#    clf = ExtraTreesRegressor(n_estimators=10, random_state=0).fit(train_data, train_label)
    pass
