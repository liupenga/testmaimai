from django.db import models
import shortuuid


# Create your models here.

def createuuid():
    return shortuuid.uuid()


class Interaction(models.Model):
    uuid = models.CharField('ID', max_length=22, primary_key=True, default=createuuid, editable=False)
    app_uuid = models.BigIntegerField("app_uuid", blank=True)
    comment_id = models.BigIntegerField("观点Id", default=0)
    like = models.BigIntegerField("关注数量", blank=True)
    spread = models.BigIntegerField("影响力", blank=True)
    comment = models.BigIntegerField("评论数", blank=True)

    @classmethod
    def new_save(cls, uid, comment_id, like, spread, comment):
        interaction = cls()
        interaction.app_uuid = uid
        interaction.comment_id = comment_id
        interaction.like = like
        interaction.spread = spread
        interaction.comment = comment
        interaction.save()


class User(models.Model):
    is_sex = ((0, "未知"), (1, "男"), (2, "女"))
    del_state_type = ((0, '已删除'), (1, '默认'))
    point_download_status = ((0, '未下载'), (1, '正在下载'), (2, '下载完成'))

    maimai_id = models.CharField("脉脉id", max_length=20, unique=True)
    uuid = models.CharField('ID', max_length=22, primary_key=True, default=createuuid, editable=False)
    app_uuid = models.BigIntegerField("app_uuid", blank=True)
    name = models.CharField("name", max_length=20)
    sex = models.BigIntegerField("性别", choices=is_sex, default=0)
    location = models.CharField("地点", max_length=50, blank=True)
    job = models.CharField("职位", max_length=20, blank=True)
    company = models.CharField("公司", max_length=20, blank=True)
    # interaction = models.ForeignKey(Interaction,verbose_name="互动")
    comment = models.CharField("评论数", max_length=5, blank=True)
    add_time = models.DateTimeField('创建时间', auto_now_add=True)
    del_state = models.IntegerField('删除状态', choices=del_state_type, default=1, db_index=True)
    avatar = models.TextField(default=None)
    company_industry = models.CharField("行业", max_length=80, blank=True)
    friends = models.IntegerField("好友数", default=0)
    opposite_friends = models.IntegerField("异性好友数", default=0)
    point_download = models.BigIntegerField("观点下载状态", choices=point_download_status, default=0)
    rank = models.IntegerField("影响力", default=0)

    def __str__(self):
        return self.name

    @classmethod
    def new_save(cls, uid, name, sex, location, job, company, avatar, company_industry, rank, friends=0,
                 opposite_friends=0):
        user = cls()
        user.name = name
        user.maimai_id = uid
        user.app_uuid = uid
        user.sex = sex
        user.location = location
        user.job = job
        user.company = company
        user.avatar = avatar
        user.company_industry = company_industry
        user.friends = friends
        user.opposite_friends = opposite_friends
        user.rank = rank
        user.save()

    def start_download_point(self):
        self.point_download = 1
        self.save()

    def finish_download_point(self):
        self.point_download = 2
        self.save()
