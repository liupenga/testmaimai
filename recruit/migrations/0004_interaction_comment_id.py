# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-16 08:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruit', '0003_auto_20170916_0710'),
    ]

    operations = [
        migrations.AddField(
            model_name='interaction',
            name='comment_id',
            field=models.BigIntegerField(default=0, verbose_name='观点Id'),
        ),
    ]
